# Juicer Social Feed

A Drupal 8 block plugin for easy integration of a juicer social wall.
The wall config is carried by the block so you can easily have several wall with different config.
See [juicer.io](https://www.juicer.io/)

## Usage

Install and enable this module the way you like.
Create an image field or use an existing one and choose "Background" fied formatter.
This module use your defined image style so don't forget to use one.

### Composer install

Past this in your composer.json repositories 
    
    {
        "type": "vcs",
        "url": "https://bitbucket.org/touali/juicersocialwall"
    }


And then
    
    composer require touali/juicersocialwall
   