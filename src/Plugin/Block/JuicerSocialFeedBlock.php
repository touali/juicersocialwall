<?php

namespace Drupal\juicersocialfeed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;

/**
 * Class JuicerSocialFeedBlock
 * Provide a 'juicersocialfeed' block.
 *
 * @package Drupal\juicersocialfeed\Block
 *
 * @Block(
 *   id = "juicersocialfeed_block",
 *   admin_label = @Translation("JuicerSocialFeed Block"),
 *   category = @Translation("JuicerSocialFeed")
 * )
 */
class JuicerSocialFeedBlock extends BlockBase  implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultConfig = \Drupal::config('juicersocialfeed.settings');

    return [
      'juicersocialfeed_feed_id' => $defaultConfig->get('juicersocialfeed.feed_id'),
      'juicersocialfeed_per' => $defaultConfig->get('juicersocialfeed.per'),
      'juicersocialfeed_pages' => $defaultConfig->get('juicersocialfeed.pages'),
      'juicersocialfeed_truncate' => $defaultConfig->get('juicersocialfeed.truncate'),
      'juicersocialfeed_gutter' => $defaultConfig->get('juicersocialfeed.gutter'),
      'juicersocialfeed_columns' => $defaultConfig->get('juicersocialfeed.columns'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $formState) {
    $form = parent::blockForm($form, $formState);

    $config = $this->getConfiguration();

    $form['juicersocialfeed_feed_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your feed id'),
      '#default_value' => isset($config['juicersocialfeed_feed_id']) ? $config['juicersocialfeed_feed_id'] : '',
    ];

    $form['juicersocialfeed_per'] = [
      '#type' => 'number',
      '#title' => $this->t('Post number'),
      '#description' => $this->t('The number of post per page you need to display.'),
      '#min' => 0,
      '#default_value' => isset($config['juicersocialfeed_per']) ? $config['juicersocialfeed_per'] : '',
    ];

    $form['juicersocialfeed_pages'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Infinite scroll limitation'),
      '#description' => $this->t('This value can be a number or infinity'),
      '#default_value' => isset($config['juicersocialfeed_pages']) ? $config['juicersocialfeed_pages'] : '',
    ];

    $form['juicersocialfeed_truncate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Items max length'),
      '#description' => $this->t('This value can be a number or infinity'),
      '#default_value' => isset($config['juicersocialfeed_truncate']) ? $config['juicersocialfeed_truncate'] : '',
    ];

    $form['juicersocialfeed_gutter'] = [
      '#type' => 'number',
      '#title' => $this->t('Gutter width'),
      '#description' => $this->t('The width between each items'),
      '#min' => 0,
      '#default_value' => isset($config['juicersocialfeed_gutter']) ? $config['juicersocialfeed_gutter'] : '',
    ];

    $form['juicersocialfeed_columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of column'),
      '#description' => $this->t('The width between each items'),
      '#min' => 0,
      '#default_value' => isset($config['juicersocialfeed_columns']) ? $config['juicersocialfeed_columns'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $formState) {
    parent::blockSubmit($form, $formState);

    $this->configuration['juicersocialfeed_feed_id'] = $formState->getValue('juicersocialfeed_feed_id');
    $this->configuration['juicersocialfeed_per'] = $formState->getValue('juicersocialfeed_per');
    $this->configuration['juicersocialfeed_pages'] = $formState->getValue('juicersocialfeed_pages');
    $this->configuration['juicersocialfeed_truncate'] = $formState->getValue('juicersocialfeed_truncate');
    $this->configuration['juicersocialfeed_gutter'] = $formState->getValue('juicersocialfeed_gutter');
    $this->configuration['juicersocialfeed_columns'] = $formState->getValue('juicersocialfeed_columns');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $juicer_attributes = new Attribute([
      'class' => 'juicer-feed',
      'data-feed-id' => empty($config['juicersocialfeed_feed_id']) ? '' : $config['juicersocialfeed_feed_id'],
      'data-per' => empty($config['juicersocialfeed_per']) ? '' : $config['juicersocialfeed_per'],
      'data-pages' => empty($config['juicersocialfeed_pages']) ? '' : $config['juicersocialfeed_pages'],
      'data-truncate' => empty($config['juicersocialfeed_truncate']) ? '' : $config['juicersocialfeed_truncate'],
      'data-gutter' => empty($config['juicersocialfeed_gutter']) ? '' : $config['juicersocialfeed_gutter'],
      'data-columns' => empty($config['juicersocialfeed_columns']) ? '' : $config['juicersocialfeed_columns'],
    ]);

    return [
      '#theme' => 'juicersocialfeed',
      '#juicer_attributes' => $juicer_attributes,
    ];
  }
}